/* eslint-disable @next/next/no-img-element */
/* eslint-disable react/no-unescaped-entities */
"use client";
import Image from "next/image";
import { EyeIcon, EyeOffIcon } from "@heroicons/react/outline";
import { useState } from "react";

export default function Home() {
  const [isPassword, setIsPassword] = useState<boolean>(false);

  return (
    <div className="h-screen">
      <div className="grid grid-cols-1 md:grid-cols-2 ">
        <div className="">
          <div className="bg-[#f5f5f3] h-screen px-5  md:px-28 lg:px-36 flex flex-col justify-center  my-auto">
            <div className="font-bold text-xl md:pl-28 top-0">SmartPOS</div>

            {/* image */}
            <div className="flex justify-center">
              <Image
                src={"/images/image.png"}
                alt="Image"
                width={700}
                height={700}
                className="h-auto w-[650px]"
              />
            </div>

            {/* header */}
            <h2 className="text-center pb-10 text-3xl md:px-28 lg:px-36  font-bold">
              Manage sales, inventory and other transactions.
            </h2>
            {/* selectors */}
            <div className="flex justify-center space-x-4">
              {/* active selector */}
              <div className="w-10 h-4 rounded-full bg-[#fec627]"></div>
              {/* inactive */}
              <div className="w-4 h-4 rounded-full bg-gray-300"></div>
              <div className="w-4 h-4 rounded-full bg-gray-300"></div>
            </div>
          </div>
        </div>
        <div className=" min-h-screen md:h-screen flex flex-col justify-between px-5 py-16 md:py-0">
          <div className="md:px-28 lg:px-36 flex flex-col justify-center h-screen my-auto">
            {/* header */}
            <h2 className="pb-2 text-4xl  font-bold">Welcome back!</h2>
            <p className="text-gray-600 text-[18px]">
              Please sign in to continue.
            </p>
            {/* form */}
            <form>
              {/* sales id number input */}
              <div className="my-8">
                <input
                  type="text"
                  placeholder="Sales ID number"
                  className="bg-[#f5f5f3] rounded-lg p-4 w-full focus:outline-none border border-gray-200"
                />
              </div>
              {/* password input */}
              <div className="my-8 relative flex">
                <input
                  type={isPassword ? "text" : "password"}
                  placeholder="Password"
                  className="bg-[#f5f5f3] rounded-lg p-4 w-full focus:outline-none border border-gray-200"
                />
                <button
                  type="button"
                  onClick={() => setIsPassword(!isPassword)}
                  className="absolute right-5 top-4"
                >
                  {isPassword ? (
                    <EyeOffIcon className="w-7 h-7 text-gray-400" />
                  ) : (
                    <EyeIcon className="w-7 h-7 text-gray-400" />
                  )}
                </button>
              </div>
              {/* submit button */}
              <button className="w-full p-4 my-8 font-bold text-xl bg-[#fec627] rounded-lg">
                Sign In
              </button>
            </form>
            <div className="flex pb-5 justify-center text-[16px] text-gray-600">
              or
            </div>
            {/* social buttons for login */}
            <div className="flex space-x-5">
              {/* google button */}
              <div className="border border-gray-300 p-1 rounded-lg flex w-1/2 items-center justify-center space-x-4">
                <img src="/images/google.png" className="w-12" alt="" />
                <span className="font-bold">Sign Up with Google</span>
              </div>
              {/* facebook button */}
              <div className="border border-gray-300 p-1 rounded-lg flex w-1/2 items-center justify-center space-x-4">
                <img src="/images/facebook.png" className="w-12" alt="" />
                <span className="font-bold">Sign Up with Facebook</span>
              </div>
            </div>

            {/* forgot password */}
            <a
              href="#"
              className="flex mt-5 justify-center  text-[16px] text-blue-600"
            >
              Forgot password?
            </a>

            <div className="flex mt-5 justify-center text-[16px] text-gray-600">
              Don't have an account? Go to registration.
            </div>
          </div>
          <p className="text-center text-gray-600 mb-5">
            {" "}
            &copy;2020 SmartPOS App
          </p>
        </div>
      </div>
    </div>
  );
}
